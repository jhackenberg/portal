module gitlab.com/jhackenberg/portal

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/iancoleman/strcase v0.2.0
	github.com/kataras/golog v0.1.7
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron/v3 v3.0.1 // indirect
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.5.4
)
