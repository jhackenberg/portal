package value

import (
	"gitlab.com/jhackenberg/portal/pkg/slices"
)

// NewResourceArray is a helper function for converting an array of resources.
func NewResourceArray(arr interface{}) []*Value {
	return slices.Map(arr, &Value{}, func(v interface{}) interface{} {
		return NewResource(v)
	}).([]*Value)
}
