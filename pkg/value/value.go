package value

import "gitlab.com/jhackenberg/portal/pkg/redirect"

type Value struct {
	Kind Kind        `json:"kind"`
	Data interface{} `json:"data"`
}

func NewResource(resource interface{}) *Value {
	return &Value{
		Kind: Resource,
		Data: resource,
	}
}

func NewRedirect(redirect *redirect.Redirect) *Value {
	return &Value{
		Kind: Redirect,
		Data: redirect,
	}
}
