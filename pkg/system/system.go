package system

type System struct {
	Arch string `json:"arch" bson:"arch"`
	OS   string `json:"os" bson:"os"`
}
