package redirect

type Redirect struct {
	URL   string `json:"url"`
	Token string `json:"token"`
}
