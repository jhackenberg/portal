package registry

type Software struct {
	ID          string `json:"id" bson:"id"`
	Name        string `json:"name" bson:"name"`
	Description string `json:"description" bson:"description"`
	Tags        []*Tag `json:"tags" bson:"tags"`
}
