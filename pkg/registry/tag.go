package registry

type Tag struct {
	Type  int    `json:"type" bson:"type"`
	Value string `json:"value" bson:"value"`
}
