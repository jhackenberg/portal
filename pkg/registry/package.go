package registry

import (
	"gitlab.com/jhackenberg/portal/pkg/command"
	"gitlab.com/jhackenberg/portal/pkg/system"
)

type Package struct {
	ID               string           `json:"id" bson:"id"`
	SoftwareID       string           `json:"softwareId" bson:"softwareId"`
	Name             string           `json:"name" bson:"name"`
	Description      string           `json:"description" bson:"description"`
	SupportedSystems []*system.System `json:"supportedSystems" bson:"supportedSystems"`
	Install          *command.Command `json:"install" bson:"install"`
	Uninstall        *command.Command `json:"uninstall" bson:"uninstall"`
}
