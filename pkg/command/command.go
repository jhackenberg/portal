package command

type Command struct {
	Program string   `json:"program" bson:"program"`
	Args    []string `json:"args" bson:"args"`
}
