package slices

import "reflect"

func Map(slice interface{}, mappedType interface{}, mapper MapperFunc) interface{} {
	value := reflect.ValueOf(slice)
	ret := reflect.MakeSlice(reflect.SliceOf(reflect.TypeOf(mappedType)), value.Len(), value.Len())
	for i := 0; i < value.Len(); i++ {
		ret.Index(i).Set(reflect.ValueOf(mapper(value.Index(i).Interface())))
	}

	return ret.Interface()
}

type MapperFunc func(v interface{}) interface{}
