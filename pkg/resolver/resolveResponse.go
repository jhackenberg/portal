package resolver

type ResolveResponse struct {
	PackageID string `json:"packageId"`
}
