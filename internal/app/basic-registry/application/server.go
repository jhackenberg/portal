package application

import "gitlab.com/jhackenberg/portal/internal/app/basic-registry/server"

func (app *App) addServer() {
	s, err := server.NewServer(app.Config.Sub("server"))
	if err != nil {
		app.Logger.Fatal(err)
	}
	app.Server = s
}
