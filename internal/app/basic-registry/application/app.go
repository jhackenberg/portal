package application

import (
	"log"

	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server"
	"gitlab.com/jhackenberg/portal/internal/pkg/application"
	"gitlab.com/jhackenberg/portal/internal/pkg/config/defaults"
)

type App struct {
	application.DefaultApp
	Server *server.Server
}

func NewApp() *App {
	app := &App{}

	if err := app.Init(application.Manifest{
		Name:           "basic-registry",
		DisplayName:    "Basic Registry",
		ConfigDefaults: []map[string]interface{}{defaults.Application},
	}); err != nil {
		log.Panicln(err)
	}
	app.addServer()

	return app
}
