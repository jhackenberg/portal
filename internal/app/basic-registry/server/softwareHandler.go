package server

import (
	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server/db"
	"gitlab.com/jhackenberg/portal/internal/pkg/registry/provider/input"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func (server *Server) softwareHandler(_ string, fields map[string]interface{}) (*value.Value, *request.Error) {
	ctx, cancel := defaultTimeoutContext()
	defer cancel()

	software := &registry.Software{}
	if err := db.FindByID(ctx, &software, server.collections.softwares, fields["softwareID"].(string)); err != nil {
		return nil, err
	}

	return value.NewResource(input.NewSoftware(software)), nil
}
