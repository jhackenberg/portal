package server

import (
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server/db"
	"gitlab.com/jhackenberg/portal/internal/pkg/registry/provider/input"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
	"go.mongodb.org/mongo-driver/bson"
)

func (server *Server) packagesHandler(_ string, _ map[string]interface{}) (*value.Value, *request.Error) {
	ctx, cancel := defaultTimeoutContext()
	defer cancel()

	var packages []*registry.Package
	if err := db.FindArray(ctx, &packages, db.NewFindContext(server.collections.packages, bson.D{})); err != nil {
		return nil, request.NewError(http.StatusInternalServerError, err)
	}

	values := make([]*value.Value, len(packages))
	for i, pkg := range packages {
		values[i] = value.NewResource(input.NewPackage(pkg))
	}

	return value.NewResource(values), nil
}
