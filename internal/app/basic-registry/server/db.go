package server

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server/db"
)

func (server *Server) addDB() error {
	dbClient, err := db.Client(server.dbConfig())
	if err != nil {
		return errors.Wrap(err, "connect to db")
	}
	server.db = dbClient

	return nil
}

func (server *Server) dbConfig() *viper.Viper {
	return server.conf.Sub("db")
}
