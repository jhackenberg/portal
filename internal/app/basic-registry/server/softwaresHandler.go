package server

import (
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server/db"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
	"go.mongodb.org/mongo-driver/bson"
)

func (server *Server) softwaresHandler(_ string, _ map[string]interface{}) (*value.Value, *request.Error) {
	ctx, cancel := defaultTimeoutContext()
	defer cancel()

	var softwares []*registry.Software
	if err := db.FindArray(ctx, &softwares, db.NewFindContext(server.collections.softwares, bson.D{})); err != nil {
		return nil, request.NewError(http.StatusInternalServerError, err)
	}

	values := make([]*value.Value, len(softwares))
	for i, software := range softwares {
		values[i] = value.NewResource(software)
	}

	return value.NewResource(values), nil
}
