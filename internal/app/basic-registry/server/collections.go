package server

func (server *Server) addCollections() {
	conf := server.dbConfig()
	database := server.db.Database(conf.GetString("name"))
	server.collections.packages = database.Collection(conf.GetString("collections.packages"))
	server.collections.softwares = database.Collection(conf.GetString("collections.softwares"))
}
