package server

import (
	"net/http"
	"os"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func (server *Server) packageDataHandler(_ string, fields map[string]interface{}) (*value.Value, *request.Error) {
	packageID := fields["packageID"].(string)
	file, err := os.Open(server.packageDataFilePath(packageID))
	if err != nil {
		return nil, request.NewError(http.StatusInternalServerError, err)
	}

	return value.NewResource(file), nil
}
