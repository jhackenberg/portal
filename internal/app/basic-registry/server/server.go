package server

import (
	"github.com/spf13/viper"
	"gitlab.com/jhackenberg/portal/internal/pkg/registry/provider"
	"go.mongodb.org/mongo-driver/mongo"
)

type Server struct {
	conf        *viper.Viper
	provider    *provider.Provider
	db          *mongo.Client
	collections struct {
		softwares *mongo.Collection
		packages  *mongo.Collection
	}
}

func NewServer(conf *viper.Viper) (*Server, error) {
	server := &Server{
		conf: conf,
	}

	if err := server.addDB(); err != nil {
		return nil, err
	}
	server.addCollections()
	server.addProvider()

	return server, nil
}

func (server *Server) ListenAndServe() error {
	return server.provider.ListenAndServe(server.conf.GetString("address"))
}
