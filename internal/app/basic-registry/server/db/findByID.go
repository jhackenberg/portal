package db

import (
	"context"
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func FindByID(ctx context.Context, v interface{}, col *mongo.Collection, id string) *request.Error {
	if err := col.FindOne(ctx, bson.D{bson.E{
		Key:   "id",
		Value: id,
	}}).Decode(v); err != nil {
		switch err {
		case mongo.ErrNoDocuments:
			return request.NewError(http.StatusNotFound, err)
		default:
			return request.NewError(http.StatusInternalServerError, err)
		}
	}

	return nil
}
