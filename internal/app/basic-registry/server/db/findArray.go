package db

import (
	"context"
	"reflect"
)

func FindArray(ctx context.Context, v interface{}, findContext *FindContext) error {
	cursor, err := findContext.Collection.Find(ctx, findContext.Filter, findContext.Options...)
	if err != nil {
		return err
	}
	itemType := reflect.TypeOf(v).Elem().Elem()
	for cursor.Next(ctx) {
		item := reflect.New(itemType).Interface()
		if err := cursor.Decode(item); err != nil {
			return err
		}
		reflect.ValueOf(v).Elem().Set(reflect.Append(reflect.ValueOf(v).Elem(), reflect.ValueOf(item).Elem()))
	}

	return nil
}
