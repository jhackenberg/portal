package db

import (
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type FindContext struct {
	Collection *mongo.Collection
	Filter     interface{}
	Options    []*options.FindOptions
}

func NewFindContext(collection *mongo.Collection, filter interface{}, options ...*options.FindOptions) *FindContext {
	return &FindContext{Collection: collection, Filter: filter, Options: options}
}
