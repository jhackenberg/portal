package server

import "gitlab.com/jhackenberg/portal/internal/pkg/registry/provider"

func (server *Server) addProvider() {
	server.provider = provider.NewProvider(&provider.Config{
		Functions: provider.Functions{
			PackagesHandler:    server.packagesHandler,
			PackageHandler:     server.packageHandler,
			PackageDataHandler: server.packageDataHandler,
			SoftwaresHandler:   server.softwaresHandler,
			SoftwareHandler:    server.softwareHandler,
		},
	})
}
