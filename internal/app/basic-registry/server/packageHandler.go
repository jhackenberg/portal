package server

import (
	"gitlab.com/jhackenberg/portal/internal/app/basic-registry/server/db"
	"gitlab.com/jhackenberg/portal/internal/pkg/registry/provider/input"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func (server *Server) packageHandler(_ string, fields map[string]interface{}) (*value.Value, *request.Error) {
	ctx, cancel := defaultTimeoutContext()
	defer cancel()

	pkg := &registry.Package{}
	if err := db.FindByID(ctx, &pkg, server.collections.packages, fields["packageID"].(string)); err != nil {
		return nil, err
	}

	return value.NewResource(input.NewPackage(pkg)), nil
}
