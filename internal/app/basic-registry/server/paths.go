package server

import "path/filepath"

func (server *Server) packageDataFilePath(packageID string) string {
	return filepath.Join(server.packageDataDir(), packageID)
}

func (server *Server) packageDataDir() string {
	return filepath.Join(server.conf.GetString("dataPath"), "packages")
}
