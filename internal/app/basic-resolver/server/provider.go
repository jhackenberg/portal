package server

import "gitlab.com/jhackenberg/portal/internal/pkg/resolver/provider"

func (server *Server) addProvider() {
	server.provider = provider.NewProvider(&provider.Config{
		Functions: provider.Functions{
			ResolveHandler: server.resolveHandler,
		},
	})
}
