package server

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/internal/pkg/resolver/provider/input"
	"gitlab.com/jhackenberg/portal/pkg/resolver"
	"gitlab.com/jhackenberg/portal/pkg/system"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func (server *Server) resolveHandler(token string, fields map[string]interface{}) (*value.Value, *request.Error) {
	softwareID := fields["softwareID"].(string)
	var t system.System
	if err := json.Unmarshal([]byte(token), &t); err != nil {
		return nil, request.NewError(http.StatusBadRequest, err)
	}
	m, ok := server.mappings[softwareID]
	if !ok {
		return nil, request.NewError(
			http.StatusNotFound,
			fmt.Errorf("no mapping for the software %q exists", softwareID),
		)
	}
	packageID, ok := m[t]
	if !ok {
		return nil, request.NewError(
			http.StatusNotFound,
			fmt.Errorf("no mapping for the system %v exists for software %q", t, softwareID),
		)
	}

	return value.NewResource(input.NewResolveResponse(&resolver.ResolveResponse{
		PackageID: packageID,
	})), nil
}
