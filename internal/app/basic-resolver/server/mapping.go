package server

import (
	"github.com/pkg/errors"
	"gitlab.com/jhackenberg/portal/pkg/system"
)

func (server *Server) addMapping() error {
	rules := map[string]map[string][]system.System{}
	if err := server.conf.UnmarshalKey("rules", &rules); err != nil {
		return errors.Wrap(err, "unmarshal rules")
	}
	mappings := map[string]map[system.System]string{}
	for softwareID, m := range rules {
		for packageID, systems := range m {
			for _, sys := range systems {
				if _, ok := mappings[softwareID]; !ok {
					mappings[softwareID] = map[system.System]string{}
				}
				mappings[softwareID][sys] = packageID
			}
		}
	}
	server.mappings = mappings

	return nil
}
