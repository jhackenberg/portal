package server

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/jhackenberg/portal/internal/pkg/resolver/provider"
	"gitlab.com/jhackenberg/portal/pkg/system"
)

type Server struct {
	conf     *viper.Viper
	provider *provider.Provider
	mappings map[string]map[system.System]string
}

func NewServer(conf *viper.Viper) (*Server, error) {
	server := &Server{
		conf: conf,
	}

	if err := server.addMapping(); err != nil {
		return nil, errors.Wrap(err, "add package mappings")
	}
	server.addProvider()

	return server, nil
}

func (server *Server) ListenAndServe() error {
	return server.provider.ListenAndServe(server.conf.GetString("address"))
}
