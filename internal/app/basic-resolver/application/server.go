package application

import "gitlab.com/jhackenberg/portal/internal/app/basic-resolver/server"

func (app *App) addServer() {
	s, err := server.NewServer(app.Config.Sub("server"))
	if err != nil {
		app.Logger.Fatal(err)
	}
	app.Server = s
}
