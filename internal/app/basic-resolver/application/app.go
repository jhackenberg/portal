package application

import (
	"log"

	"gitlab.com/jhackenberg/portal/internal/app/basic-resolver/server"
	"gitlab.com/jhackenberg/portal/internal/pkg/application"
	"gitlab.com/jhackenberg/portal/internal/pkg/config/defaults"
)

type App struct {
	application.DefaultApp
	Server *server.Server
}

func NewApp() *App {
	app := &App{}

	if err := app.Init(application.Manifest{
		Name:           "basic-resolver",
		DisplayName:    "Basic Resolver",
		ConfigDefaults: []map[string]interface{}{defaults.Application},
	}); err != nil {
		log.Panicln(err)
	}
	app.addServer()

	return app
}
