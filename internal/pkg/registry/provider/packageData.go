package provider

import (
	"encoding/json"
	"io"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func (provider *Provider) packageDataHandlerFunc() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Portal-Token")
		vars := mux.Vars(r)
		res, err := provider.functions.PackageDataHandler(token, map[string]interface{}{
			"packageID": vars["packageID"],
		})
		if err != nil {
			err.Write(w)
			return
		}
		switch res.Kind {
		case value.Resource:
			rc := res.Data.(io.ReadCloser)
			defer func(rc io.ReadCloser) { _ = rc.Close() }(rc)
			w.Header().Set("Content-Type", "application/octet-stream")
			if _, err := io.Copy(w, rc); err != nil {
				request.NewError(http.StatusInternalServerError, err).Write(w)
				return
			}
		case value.Redirect:
			w.Header().Set("Content-Type", "application/json")
			if err := json.NewEncoder(w).Encode(res); err != nil {
				request.NewError(http.StatusInternalServerError, err).Write(w)
				return
			}
		default:
			request.NewError(http.StatusInternalServerError, errors.Errorf("unknown value kind %d", res.Kind)).Write(w)
		}
	}
}
