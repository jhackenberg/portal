package provider

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Provider struct {
	router    *mux.Router
	functions Functions
}

func NewProvider(conf *Config) *Provider {
	provider := &Provider{
		functions: conf.Functions,
	}

	provider.registerRoutes()

	return provider
}

func (provider *Provider) ListenAndServe(addr string) error {
	return http.ListenAndServe(addr, provider.router)
}
