package provider

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/internal/pkg/testing/response"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestProvider_packageDataHandlerFunc(t *testing.T) {
	type fields struct {
		functions Functions
	}
	type args struct {
		packageID string
		token     string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *http.Response
	}{
		{
			name: "populated",
			fields: fields{
				functions: Functions{
					PackageDataHandler: populatedPackageDataHandler,
				},
			},
			args: args{
				token: "test",
			},
			want: &http.Response{
				StatusCode: 200,
				Body:       io.NopCloser(bytes.NewBuffer(populatedPackageData())),
				Header: map[string][]string{
					"Content-Type": {"application/octet-stream"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			provider := &Provider{
				functions: tt.fields.functions,
			}
			rec := httptest.NewRecorder()
			h := provider.packageDataHandlerFunc()
			req := httptest.NewRequest("", "/packages/"+tt.args.packageID+"/data", nil)
			req.Header.Set("Portal-Token", tt.args.token)
			h(rec, req)
			response.AssertEqual(t, tt.want, rec.Result())
		})
	}
}

func populatedPackageData() []byte {
	return []byte{}
}

func populatedPackageDataHandler(token string, _ map[string]interface{}) (*value.Value, *request.Error) {
	if token != "test" {
		panic(fmt.Sprintf("token should be %q, got %q", "test", token))
	}

	return value.NewResource(io.NopCloser(bytes.NewReader(populatedPackageData()))), nil
}
