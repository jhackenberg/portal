package provider

import (
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
)

func (provider *Provider) softwaresHandlerFunc() http.HandlerFunc {
	return request.BasicHandlerFunc(provider.functions.SoftwaresHandler, nil)
}
