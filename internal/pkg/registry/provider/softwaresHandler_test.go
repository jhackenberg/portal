package provider

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/internal/pkg/testing/response"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestProvider_softwaresHandlerFunc(t *testing.T) {
	type fields struct {
		functions Functions
	}
	type args struct {
		token string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *http.Response
	}{
		{
			name: "populated",
			fields: fields{
				functions: Functions{
					SoftwaresHandler: populatedSoftwaresHandler,
				},
			},
			args: args{
				token: "test",
			},
			want: &http.Response{
				StatusCode: 200,
				Body:       io.NopCloser(bytes.NewBuffer(populatedSoftwaresJSON())),
				Header: map[string][]string{
					"Content-Type": {"application/json"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			provider := &Provider{
				functions: tt.fields.functions,
			}
			rec := httptest.NewRecorder()
			h := provider.softwaresHandlerFunc()
			req := httptest.NewRequest("", "/softwares", nil)
			req.Header.Set("Portal-Token", tt.args.token)
			h(rec, req)
			response.AssertEqual(t, tt.want, rec.Result())
		})
	}
}

var populatedSoftwaresValue = value.NewResource([]*value.Value{populatedSoftwareValue})

func populatedSoftwaresHandler(token string, _ map[string]interface{}) (*value.Value, *request.Error) {
	if token != "test" {
		panic(fmt.Sprintf("token should be %q, got %q", "test", token))
	}

	return populatedSoftwaresValue, nil
}

func populatedSoftwaresJSON() []byte {
	ret, _ := json.Marshal(populatedSoftwaresValue)
	return append(ret, '\n')
}
