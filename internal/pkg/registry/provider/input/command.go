package input

import (
	"gitlab.com/jhackenberg/portal/pkg/command"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type Command struct {
	Program *value.Value `json:"program"`
	Args    *value.Value `json:"args"`
}

func NewCommand(command *command.Command) *Command {
	return &Command{
		Program: value.NewResource(command.Program),
		Args:    value.NewResource(value.NewResourceArray(command.Args)),
	}
}
