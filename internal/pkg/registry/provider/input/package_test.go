package input

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jhackenberg/portal/pkg/command"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/system"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestNewPackage(t *testing.T) {
	type args struct {
		pkg *registry.Package
	}
	tests := []struct {
		name string
		args args
		want *Package
	}{
		{
			name: "populated",
			args: args{
				pkg: &registry.Package{
					ID:          "1",
					SoftwareID:  "1",
					Name:        "foo",
					Description: "",
					SupportedSystems: []*system.System{
						{
							Arch: "someArch",
							OS:   "someOS",
						},
					},
					Install: &command.Command{
						Program: "install",
						Args:    nil,
					},
					Uninstall: &command.Command{
						Program: "uninstall",
						Args:    nil,
					},
				},
			},
			want: &Package{
				ID: &value.Value{
					Kind: 0,
					Data: "1",
				},
				SoftwareID: &value.Value{
					Kind: 0,
					Data: "1",
				},
				Name: &value.Value{
					Kind: 0,
					Data: "foo",
				},
				Description: &value.Value{
					Kind: 0,
					Data: "",
				},
				SupportedSystems: &value.Value{
					Kind: 0,
					Data: []*value.Value{
						{
							Kind: 0,
							Data: &System{
								Arch: &value.Value{
									Kind: 0,
									Data: "someArch",
								},
								OS: &value.Value{
									Kind: 0,
									Data: "someOS",
								},
							},
						},
					},
				},
				Install: &value.Value{
					Kind: 0,
					Data: &Command{
						Program: &value.Value{
							Kind: 0,
							Data: "install",
						},
						Args: &value.Value{
							Kind: 0,
							Data: []*value.Value{},
						},
					},
				},
				Uninstall: &value.Value{
					Kind: 0,
					Data: &Command{
						Program: &value.Value{
							Kind: 0,
							Data: "uninstall",
						},
						Args: &value.Value{
							Kind: 0,
							Data: []*value.Value{},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, NewPackage(tt.args.pkg))
		})
	}
}
