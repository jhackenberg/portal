package input

import (
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type Tag struct {
	Type  *value.Value `json:"type"`
	Value *value.Value `json:"value"`
}

func NewTagArray(tags []*registry.Tag) []*Tag {
	ret := make([]*Tag, len(tags))
	for i, tag := range tags {
		ret[i] = NewTag(tag)
	}

	return ret
}

func NewTag(tag *registry.Tag) *Tag {
	return &Tag{
		Type:  value.NewResource(tag.Type),
		Value: value.NewResource(tag.Value),
	}
}
