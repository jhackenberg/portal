package input

import (
	"gitlab.com/jhackenberg/portal/pkg/system"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type System struct {
	Arch *value.Value
	OS   *value.Value
}

func NewSystem(system *system.System) *System {
	return &System{
		Arch: value.NewResource(system.Arch),
		OS:   value.NewResource(system.OS),
	}
}
