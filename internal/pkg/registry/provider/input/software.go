package input

import (
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type Software struct {
	ID          *value.Value `json:"id"`
	Name        *value.Value `json:"name"`
	Description *value.Value `json:"description"`
	Tags        *value.Value `json:"tags"`
}

func NewSoftware(software *registry.Software) *Software {
	return &Software{
		ID:          value.NewResource(software.ID),
		Name:        value.NewResource(software.Name),
		Description: value.NewResource(software.Description),
		Tags:        value.NewResource(value.NewResourceArray(NewTagArray(software.Tags))),
	}
}
