package input

import (
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/slices"
	"gitlab.com/jhackenberg/portal/pkg/system"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type Package struct {
	ID               *value.Value `json:"id"`
	SoftwareID       *value.Value `json:"softwareId"`
	Name             *value.Value `json:"name"`
	Description      *value.Value `json:"description"`
	SupportedSystems *value.Value `json:"supportedSystems"`
	Install          *value.Value `json:"install"`
	Uninstall        *value.Value `json:"uninstall"`
}

func NewPackage(pkg *registry.Package) *Package {
	return &Package{
		ID:          value.NewResource(pkg.ID),
		SoftwareID:  value.NewResource(pkg.SoftwareID),
		Name:        value.NewResource(pkg.Name),
		Description: value.NewResource(pkg.Description),
		SupportedSystems: value.NewResource(slices.Map(pkg.SupportedSystems, &value.Value{}, func(v interface{}) interface{} {
			return value.NewResource(NewSystem(v.(*system.System)))
		})),
		Install:   value.NewResource(NewCommand(pkg.Install)),
		Uninstall: value.NewResource(NewCommand(pkg.Uninstall)),
	}
}
