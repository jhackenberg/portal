package input

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jhackenberg/portal/pkg/registry"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestNewSoftware(t *testing.T) {
	type args struct {
		software *registry.Software
	}
	tests := []struct {
		name string
		args args
		want *Software
	}{
		{
			name: "populated",
			args: args{
				software: &registry.Software{
					ID:          "1",
					Name:        "foo",
					Description: "",
					Tags: []*registry.Tag{
						{
							Type:  0,
							Value: "some tag",
						},
					},
				},
			},
			want: &Software{
				ID: &value.Value{
					Kind: 0,
					Data: "1",
				},
				Name: &value.Value{
					Kind: 0,
					Data: "foo",
				},
				Description: &value.Value{
					Kind: 0,
					Data: "",
				},
				Tags: &value.Value{
					Kind: 0,
					Data: []*value.Value{
						{
							Kind: 0,
							Data: &Tag{
								Type: &value.Value{
									Kind: 0,
									Data: 0,
								},
								Value: &value.Value{
									Kind: 0,
									Data: "some tag",
								},
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, NewSoftware(tt.args.software))
		})
	}
}
