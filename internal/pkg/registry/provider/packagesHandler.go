package provider

import (
	"net/http"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
)

func (provider *Provider) packagesHandlerFunc() http.HandlerFunc {
	return request.BasicHandlerFunc(provider.functions.PackagesHandler, nil)
}
