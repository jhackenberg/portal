package provider

import "github.com/gorilla/mux"

func (provider *Provider) registerRoutes() {
	provider.router = mux.NewRouter()
	provider.router.HandleFunc("/packages", provider.packagesHandlerFunc())
	provider.router.HandleFunc("/packages/{packageID}", provider.packageHandlerFunc())
	provider.router.HandleFunc("/packages/{packageID}/data", provider.packageDataHandlerFunc())
	provider.router.HandleFunc("/softwares", provider.softwaresHandlerFunc())
	provider.router.HandleFunc("/softwares/{softwareID}", provider.softwareHandlerFunc())
}
