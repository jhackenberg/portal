package provider

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/jhackenberg/portal/internal/pkg/request"
)

func (provider *Provider) packageHandlerFunc() http.HandlerFunc {
	return request.BasicHandlerFunc(
		provider.functions.PackageHandler,
		func(token string, r *http.Request) (map[string]interface{}, *request.Error) {
			vars := mux.Vars(r)
			return map[string]interface{}{
				"packageID": vars["packageID"],
			}, nil
		},
	)
}
