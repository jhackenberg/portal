package application

type Manifest struct {
	Name           string
	DisplayName    string
	ConfigDefaults []map[string]interface{}
}
