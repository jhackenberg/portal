package provider

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewProvider(t *testing.T) {
	type args struct {
		conf *Config
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "zero",
			args: args{
				conf: &Config{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			provider := NewProvider(tt.args.conf)
			assert.NotNil(t, provider)
			assert.NotNil(t, provider.router)
		})
	}
}
