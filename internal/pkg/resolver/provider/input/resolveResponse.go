package input

import (
	"gitlab.com/jhackenberg/portal/pkg/resolver"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type ResolveResponse struct {
	PackageID *value.Value `json:"packageId"`
}

func NewResolveResponse(response *resolver.ResolveResponse) *ResolveResponse {
	return &ResolveResponse{
		PackageID: value.NewResource(response.PackageID),
	}
}
