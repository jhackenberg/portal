package input

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jhackenberg/portal/pkg/resolver"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestNewResolveResponse(t *testing.T) {
	type args struct {
		response *resolver.ResolveResponse
	}
	tests := []struct {
		name string
		args args
		want *ResolveResponse
	}{
		{
			name: "populated",
			args: args{
				response: &resolver.ResolveResponse{PackageID: "1"},
			},
			want: &ResolveResponse{
				PackageID: &value.Value{
					Kind: 0,
					Data: "1",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, NewResolveResponse(tt.args.response))
		})
	}
}
