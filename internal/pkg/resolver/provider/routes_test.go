package provider

import (
	"testing"

	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

func TestProvider_registerRoutes(t *testing.T) {
	provider := &Provider{
		router: mux.NewRouter(),
	}
	requiredRoutes := map[string]bool{
		"/resolve/{softwareID}": true,
	}
	provider.registerRoutes()
	_ = provider.router.Walk(func(route *mux.Route, _ *mux.Router, _ []*mux.Route) error {
		template, _ := route.GetPathTemplate()
		delete(requiredRoutes, template)
		return nil
	})
	assert.Empty(t, requiredRoutes)
}
