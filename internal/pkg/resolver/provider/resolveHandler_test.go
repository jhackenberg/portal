package provider

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/jhackenberg/portal/internal/pkg/request"
	"gitlab.com/jhackenberg/portal/internal/pkg/resolver/provider/input"
	"gitlab.com/jhackenberg/portal/internal/pkg/testing/response"
	"gitlab.com/jhackenberg/portal/pkg/resolver"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestProvider_packageHandlerFunc(t *testing.T) {
	type fields struct {
		functions Functions
	}
	type args struct {
		packageID string
		token     string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *http.Response
	}{
		{
			name: "populated",
			fields: fields{
				functions: Functions{
					ResolveHandler: populatedResolveHandler,
				},
			},
			args: args{
				token: "test",
			},
			want: &http.Response{
				StatusCode: 200,
				Body:       io.NopCloser(bytes.NewBuffer(populatedResolveResponseJSON())),
				Header: map[string][]string{
					"Content-Type": {"application/json"},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			provider := &Provider{
				functions: tt.fields.functions,
			}
			rec := httptest.NewRecorder()
			h := provider.resolveHandlerFunc()
			req := httptest.NewRequest("", "/packages/"+tt.args.packageID, nil)
			req.Header.Set("Portal-Token", tt.args.token)
			h(rec, req)
			response.AssertEqual(t, tt.want, rec.Result())
		})
	}
}

func populatedResolveResponseJSON() []byte {
	ret, _ := json.Marshal(populatedResolveResponseValue)
	return append(ret, '\n')
}

var populatedResolveResponseValue = value.NewResource(input.NewResolveResponse(&resolver.ResolveResponse{
	PackageID: "1",
}))

func populatedResolveHandler(token string, _ map[string]interface{}) (*value.Value, *request.Error) {
	if token != "test" {
		panic(fmt.Sprintf("token should be %q, got %q", "test", token))
	}

	return populatedResolveResponseValue, nil
}
