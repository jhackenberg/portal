package provider

import "gitlab.com/jhackenberg/portal/internal/pkg/request"

type Config struct {
	Functions Functions
}

type Functions struct {
	ResolveHandler request.Handler
}
