package provider

import "github.com/gorilla/mux"

func (provider *Provider) registerRoutes() {
	provider.router = mux.NewRouter()

	provider.router.HandleFunc("/resolve/{softwareID}", provider.resolveHandlerFunc())
}
