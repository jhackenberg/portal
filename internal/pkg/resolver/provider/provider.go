package provider

import (
	"net/http"

	"github.com/gorilla/mux"
)

type Provider struct {
	router    *mux.Router
	functions Functions
}

func NewProvider(conf *Config) *Provider {
	resolver := &Provider{
		functions: conf.Functions,
	}

	resolver.registerRoutes()

	return resolver
}

func (provider *Provider) ListenAndServe(addr string) error {
	return http.ListenAndServe(addr, provider.router)
}
