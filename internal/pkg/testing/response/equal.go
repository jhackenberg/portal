package response

import (
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func AssertEqual(t *testing.T, expected *http.Response, actual *http.Response) {
	assert.Equal(t, expected.StatusCode, actual.StatusCode)
	if expected.Header != nil {
		assert.Equal(t, expected.Header, actual.Header)
	}
	if expected.Body != nil {
		expectedBody, _ := ioutil.ReadAll(expected.Body)
		actualBody, _ := ioutil.ReadAll(actual.Body)
		assert.Equal(t, string(expectedBody), string(actualBody))
	}
	if expected.Trailer != nil {
		assert.Equal(t, expected.Trailer, actual.Trailer)
	}
}
