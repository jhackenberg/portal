package request

import (
	"fmt"
	"net/http"
)

type Error struct {
	StatusCode int
	Err        error
}

func NewError(statusCode int, err error) *Error {
	return &Error{
		StatusCode: statusCode,
		Err:        err,
	}
}

func (e *Error) Error() string {
	return fmt.Sprintf("%s (%d)", e.Err, e.StatusCode)
}

func (e *Error) Write(w http.ResponseWriter) {
	http.Error(w, e.Err.Error(), e.StatusCode)
}
