package request

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/jhackenberg/portal/internal/pkg/testing/response"
	"gitlab.com/jhackenberg/portal/pkg/value"
)

func TestBasicHandlerFunc(t *testing.T) {
	type args struct {
		h          Handler
		fieldsFunc fieldsFunc
		token      string
	}
	tests := []struct {
		name string
		args args
		want *http.Response
	}{
		{
			name: "full",
			args: args{
				h:          fullHandler,
				fieldsFunc: fullFieldsFunc,
				token:      "test",
			},
			want: &http.Response{
				StatusCode: 200,
				Header: map[string][]string{
					"Content-Type": {"application/json"},
				},
				Body: io.NopCloser(bytes.NewBuffer(fullValueJSON())),
			},
		},
		{
			name: "fieldsError",
			args: args{
				fieldsFunc: fieldsErrorFieldsFunc,
			},
			want: &http.Response{
				StatusCode: 500,
			},
		},
		{
			name: "handlerError",
			args: args{
				h: handlerErrorHandler,
			},
			want: &http.Response{
				StatusCode: 500,
			},
		},
		{
			name: "encodeError",
			args: args{
				h: encodeErrorHandler,
			},
			want: &http.Response{
				StatusCode: 500,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			rec := httptest.NewRecorder()
			handler := BasicHandlerFunc(tt.args.h, tt.args.fieldsFunc)
			req := httptest.NewRequest("", "/", nil)
			req.Header.Set("Portal-Token", tt.args.token)
			handler(rec, req)
			response.AssertEqual(t, tt.want, rec.Result())
		})
	}
}

var fullValue = value.NewResource("test")

func fullValueJSON() []byte {
	ret, _ := json.Marshal(fullValue)
	return append(ret, '\n')
}

func fullHandler(token string, fields map[string]interface{}) (*value.Value, *Error) {
	if token != "test" {
		panic(fmt.Sprintf("token should be %q, got %q", "test", token))
	}
	expectedFields := map[string]interface{}{
		"foo":   "bar",
		"token": token,
	}
	if !reflect.DeepEqual(fields, expectedFields) {
		panic(fmt.Sprintf("fields should be %v, got %v", expectedFields, fields))
	}

	return fullValue, nil
}

func fullFieldsFunc(token string, _ *http.Request) (map[string]interface{}, *Error) {
	return map[string]interface{}{
		"token": token,
		"foo":   "bar",
	}, nil
}

func fieldsErrorFieldsFunc(_ string, _ *http.Request) (map[string]interface{}, *Error) {
	return nil, NewError(500, errors.New("oh noes"))
}

func handlerErrorHandler(_ string, _ map[string]interface{}) (*value.Value, *Error) {
	return nil, NewError(500, errors.New("oh noes"))
}

func encodeErrorHandler(_ string, _ map[string]interface{}) (*value.Value, *Error) {
	return value.NewResource(MarshalErrorCause{}), nil
}

type MarshalErrorCause struct{}

func (m MarshalErrorCause) MarshalJSON() ([]byte, error) {
	return nil, errors.New("oh noes")
}
