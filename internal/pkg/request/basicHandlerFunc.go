package request

import (
	"encoding/json"
	"net/http"
)

func BasicHandlerFunc(h Handler, fieldsFunc fieldsFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Portal-Token")
		var fields map[string]interface{}
		var err *Error
		if fieldsFunc != nil {
			fields, err = fieldsFunc(token, r)
			if err != nil {
				err.Write(w)
				return
			}
		}
		res, err := h(token, fields)
		if err != nil {
			err.Write(w)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(w).Encode(res); err != nil {
			NewError(http.StatusInternalServerError, err).Write(w)
			return
		}
	}
}

type fieldsFunc func(token string, r *http.Request) (map[string]interface{}, *Error)
