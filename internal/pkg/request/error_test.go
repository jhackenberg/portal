package request

import (
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
)

func TestNewError(t *testing.T) {
	type args struct {
		statusCode int
		err        error
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "zero",
			args: args{},
		},
		{
			name: "populated",
			args: args{
				statusCode: http.StatusInternalServerError,
				err:        errors.New("something went wrong"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.NotNil(t, NewError(tt.args.statusCode, tt.args.err))
		})
	}
}
