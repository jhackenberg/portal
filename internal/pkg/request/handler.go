package request

import (
	"gitlab.com/jhackenberg/portal/pkg/value"
)

type Handler func(token string, fields map[string]interface{}) (*value.Value, *Error)
