package main

import "gitlab.com/jhackenberg/portal/internal/app/basic-resolver/application"

func main() {
	app := application.NewApp()
	if err := app.Server.ListenAndServe(); err != nil {
		app.Logger.Fatal(err)
	}
}
